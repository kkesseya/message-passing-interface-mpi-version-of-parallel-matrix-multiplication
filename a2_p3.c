/*Name: Kwaku Kessey-Ankomah Jr
Assignment 1
*/
#define  _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

void setMatrixA_Dimen();
void setMatrixB_Dimen();
void setMatrixC_Dimen();
void fillMatrixA();
void fillMatrixB();
int malloc2dInt(int ***array, int n, int m);
int malloc2dIntC(int ***array, int n, int m);
int free2dInt(int ***array);
void validateNumThreadsArg();
void multiRoutine(int startRow);
void multiRoutineFirst();


int numPThreads = 1; //default value for # Pthreads used
char *line = NULL; //ptr for stdin
int dimensionA[2];
int dimensionB[2];
int c, bytesread; //dimensions for matrix A and B
size_t len = 0;
int ** matrixC; //declare global 2d array matrixC
int ** matrixOut; //declare global 2d array matrixC
int ** matrixB; //declare global 2d array matrixB
int ** matrixA; //declare global 2d array matrixA
int tag = 0,numTasks,rank=0,rowsPerTask, target, source;
int offset = 0;
FILE *fp;
MPI_Status status;
double start, end;


int main (int argc, char *argv[])
{
	MPI_Init(&argc,&argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);
    numPThreads = numTasks;

	MPI_Barrier(MPI_COMM_WORLD); /* IMPORTANT */
	start = MPI_Wtime();

	if(rank  == 0){
		//does a quantity validation check on arguments provided

		if (argc != 2) {
			fprintf(stderr, "input matrix are required as arguments\n");
			exit(1);
		}

		fp = fopen(argv[1], "r");

		if (!fp) {
			fprintf(stderr, "Error opening file '%s'\n", argv[1]);
			exit(1);
		}



		setMatrixA_Dimen(); //get matrixA's dimensions 
		malloc2dInt(&matrixA, dimensionA[0], dimensionA[1]);

		validateNumThreadsArg();

		fillMatrixA(); //fill matrixA from values from stdin 
		setMatrixB_Dimen();
		malloc2dInt(&matrixB, dimensionB[0], dimensionB[1]);
		fillMatrixB();
		free(line);
		rowsPerTask = dimensionA[0]/numTasks;
		target=0;
		offset = rowsPerTask;
		int tempRowsPerTask;
	// 	//---Sends data to all task managers


		for(target=1; target<numTasks;target++){
//			printf("Master: 0 sends data(matrixs operands) to node %d\n", target);
			MPI_Send(dimensionB,2,MPI_INT,target,0,MPI_COMM_WORLD);
			MPI_Send(dimensionA,2,MPI_INT,target,0,MPI_COMM_WORLD);
			MPI_Send(&(matrixB[0][0]),dimensionB[0]*dimensionB[1],MPI_INT,target,0,MPI_COMM_WORLD);
			MPI_Send(&offset,1,MPI_INT,target,0,MPI_COMM_WORLD);
			if(target == (numTasks-1)){
				tempRowsPerTask = dimensionA[0]-offset;
				MPI_Send(&tempRowsPerTask,1,MPI_INT,target,0,MPI_COMM_WORLD);
				MPI_Send(&(matrixA[offset][0]), (dimensionA[0]-offset)*dimensionA[1],MPI_INT,target,0,MPI_COMM_WORLD);
			}else{
				MPI_Send(&rowsPerTask,1,MPI_INT,target,0,MPI_COMM_WORLD);
				MPI_Send(&(matrixA[offset][0]), rowsPerTask*dimensionA[1],MPI_INT,target,0,MPI_COMM_WORLD);
			}
			offset += rowsPerTask;
		}
		malloc2dInt(&matrixOut, dimensionA[0], dimensionB[1]);
		multiRoutineFirst();//processing


		source=0;
		// --recieves data from all task managers
		for(source=1; source<numTasks;source++){
//			printf("Master: 0 recieved matrix results from Node: %d\n", source);
			MPI_Recv(&offset,1,MPI_INT,source,1,MPI_COMM_WORLD,&status);
			MPI_Recv(&rowsPerTask,1,MPI_INT,source,1,MPI_COMM_WORLD,&status);
			MPI_Recv(&(matrixOut[offset][0]), rowsPerTask*dimensionA[1],MPI_INT,source,1,MPI_COMM_WORLD,&status);
		}
		
	}

	if(rank > 0){
//		printf("Node: %d recieved data from Master: 0\n", rank);
		source = 0;
		MPI_Recv(dimensionB,2,MPI_INT,source,0,MPI_COMM_WORLD,&status);
		MPI_Recv(dimensionA,2,MPI_INT,source,0,MPI_COMM_WORLD,&status);
		malloc2dInt(&matrixB, dimensionB[0], dimensionB[1]);
		MPI_Recv(&(matrixB[0][0]),dimensionB[0]*dimensionB[1],MPI_INT,source,0,MPI_COMM_WORLD,&status);
		MPI_Recv(&offset,1,MPI_INT,source,0,MPI_COMM_WORLD,&status);
		MPI_Recv(&rowsPerTask,1,MPI_INT,source,0,MPI_COMM_WORLD,&status);
		dimensionA[0] = rowsPerTask;
		malloc2dInt(&matrixA, rowsPerTask, dimensionA[1]);
		MPI_Recv(&(matrixA[0][0]), rowsPerTask*dimensionA[1],MPI_INT,source,0,MPI_COMM_WORLD,&status);
		malloc2dInt(&matrixC, rowsPerTask, dimensionB[1]);

		multiRoutine(0);//processing
		target = 0;
		MPI_Send(&offset,1,MPI_INT,target,1,MPI_COMM_WORLD);
		MPI_Send(&rowsPerTask,1,MPI_INT,target,1,MPI_COMM_WORLD);
		MPI_Send(&(matrixC[0][0]), rowsPerTask*dimensionB[1], MPI_INT,target,1,MPI_COMM_WORLD);
		free2dInt(&matrixC);
	}

		

	free2dInt(&matrixA);
	free2dInt(&matrixB);
	
	MPI_Barrier(MPI_COMM_WORLD); /* IMPORTANT */
	end = MPI_Wtime();

	if(rank==0){
/*	printf("amount of rows: %d", dimensionA[0]);  
      	 int r = 0;
	     int d = 0;
	     for(d; d<dimensionA[0]; d++){
	     	for(r=0;r<dimensionB[1];r++){
	     			printf("%d  ",matrixOut[d][r]);
	     	}
	     	printf("\n");
	     }*/
	    free2dInt(&matrixOut);

	}

	MPI_Finalize();

	if (rank == 0) { /* use time on master node */
 	   printf("Runtime = %f\n", end-start);
	}
    return 0;
}

int malloc2dInt(int ***array, int n, int m) {

    /* allocate the n*m contiguous items */
    int *p = (int *)malloc(n*m*sizeof(int));
    if (!p) return -1;

    /* allocate the row pointers into the memory */
    (*array) = (int **)malloc(n*sizeof(int*));
    if (!(*array)) {
       free(p);
       return -1;
    }

    /* set up the pointers into the contiguous memory */
    for (int i=0; i<n; i++) 
       (*array)[i] = &(p[i*m]);

    return 0;
}

int malloc2dIntC(int ***array, int n, int m) {

    /* allocate the n*m contiguous items */
    int *p = (int *)calloc(n*m,sizeof(int));
    if (!p) return -1;

    /* allocate the row pointers into the memory */
    (*array) = (int **)malloc(n*sizeof(int*));
    if (!(*array)) {
       free(p);
       return -1;
    }

    /* set up the pointers into the contiguous memory */
    for (int i=0; i<n; i++) 
       (*array)[i] = &(p[i*m]);

    return 0;
}

int free2dInt(int ***array) {
    /* free the memory - the first element of the array is at the start */
    free(&((*array)[0][0]));

    /* free the pointers into the memory */
    free(*array);

    return 0;
}


void multiRoutineFirst(){
	int n, sum = 0; //the counter and sum
	int v;
	int k; //the starting row for this thread
	for(k= 0; k<rowsPerTask;k++){//rows in Matrix A
		for(v = 0; v < dimensionB[1]; v++){//columns in Matrix B
			//Row multiplied by column
			sum = 0;
			for(n = 0; n<dimensionA[1]; n++){
				sum += matrixA[k][n] * matrixB[n][v];
			}
			matrixOut[k][v] = sum; //sum to its coordinates
		}
	}
}

void multiRoutine(int startRow) {
	int n, sum = 0; //the counter and sum
	int v;
	int k; //the starting row for this thread
	for(k= 0; k<dimensionA[0];k++){//rows in Matrix A
		for(v = 0; v < dimensionB[1]; v++){//columns in Matrix B
			//Row multiplied by column
			sum = 0;
			for(n = 0; n<dimensionA[1]; n++){
				sum += matrixA[k][n] * matrixB[n][v];
			}
			matrixC[k][v] = sum; //sum to its coordinates
		}
	}
}


void fillMatrixA(){
	char* input1;

    int rowNum = 0;
    int colNum = 0;
	while (getline(&line, &len, fp) != -1) {
		input1 = line;
		colNum = 0;
	    while (sscanf(input1, "%d%n", &c, &bytesread) > 0) {
	        matrixA[rowNum][colNum++] = c;
	        input1 += bytesread;
	    }

		if(rowNum >= (dimensionA[0]-1)){
			break;//array a has been filled
		}
		rowNum++; //file lineNum

	}

}

void fillMatrixB(){
	char* input1;

    int rowNum = 0;
    int colNum = 0;
	while (getline(&line, &len, fp) != -1) {
		
		input1 = line;
		colNum = 0;
	    while (sscanf(input1, "%d%n", &c, &bytesread) > 0) {
	        matrixB[rowNum][colNum++] = c;
	        input1 += bytesread;
	    }

		if(rowNum >= (dimensionB[0]-1)){
			break;//array a has been filled
		}
		rowNum++; //file lineNum

	}

}

void setMatrixA_Dimen(){
	if(getline(&line, &len, fp) != -1){
		char* input1 = line;
		int length = 0;
	    while (sscanf(input1, "%d%n", &c, &bytesread) > 0) {
	        dimensionA[length++] = c;
	        input1 += bytesread;
	    }
	}

}

void setMatrixB_Dimen(){
	if(getline(&line, &len, fp) != -1){
		char* input1 = line;
		int length = 0;
	    while (sscanf(input1, "%d%n", &c, &bytesread) > 0) {
	        dimensionB[length++] = c;
	        input1 += bytesread;
	    }
    }

    if(dimensionA[1] != dimensionB[0]){
    	fprintf(stderr, "Dimensions of matrix A and B are incompatible\n");
    	free(line);
		exit(1);
    }
	
}

void setMatrixC_Dimen(){
    matrixC = (int **)malloc(sizeof(int *)*dimensionA[0]);
	for(int i = 0; i<dimensionA[0]; i++){
		matrixC[i] = calloc(dimensionB[1], sizeof(int));
	}
}

void validateNumThreadsArg(){
	if(numPThreads > dimensionA[0]){ //validate the amount of pThreads specified
		fprintf(stderr, "Number of PThreads can not exceed %d threads(limiting factor is MAtrixA's max rows).\n", dimensionA[0]);
		free(line);
		exit(1);
	}
}
